Name:        gpac
Summary:     MPEG-4 multimedia framework
Version:     2.0.0
Release:     1
License:     LGPLv2+
URL:         https://gpac.sourceforge.net/
Source0:     https://github.com/gpac/gpac/archive/v%{version}/gpac-%{version}.tar.gz

BuildRequires:  SDL2-devel
BuildRequires:  librsvg2-devel >= 2.5.0
BuildRequires:  libGLU-devel
BuildRequires:  freetype-devel >= 2.1.4
BuildRequires:  faad2-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libpng-devel >= 1.2.5
BuildRequires:  libmad-devel
BuildRequires:  xvidcore-devel >= 1.0.0
BuildRequires:  ffmpeg-devel
BuildRequires:  libxml2-devel
BuildRequires:  openssl-devel
BuildRequires:  openjpeg2-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  zlib-devel
BuildRequires:  libogg-devel
BuildRequires:  libvorbis-devel
BuildRequires:  libtheora-devel
BuildRequires:  libXt-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXv-devel
BuildRequires:  xmlrpc-c-devel
BuildRequires:  doxygen graphviz
BuildRequires:  gcc-c++

%description
GPAC is a multimedia framework based on the MPEG-4 Systems standard developed
from scratch in ANSI C.  The original development goal is to provide a clean,
small and flexible alternative to the MPEG-4 Systems reference software.

GPAC features the integration of recent multimedia standards (SVG/SMIL, VRML,
X3D, SWF, 3GPP(2) tools and more) into a single framework. GPAC also features
MPEG-4 Systems encoders/multiplexers, publishing tools for content distribution
for MP4 and 3GPP(2) files and many tools for scene descriptions
(MPEG4 <-> VRML <-> X3D converters, SWF -> MPEG-4, etc).

%package        libs
Summary:        Library for %{name}

%description    libs
The %{name}-libs package contains library for %{name}.

%package  devel
Summary:  Development libraries and files for %{name}
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description  devel
Development libraries and files for gpac.

%package  doc
Summary:  Documentation for %{name}

%description  doc
Documentation for %{name}.

%package  static
Summary:  Development libraries and files for %{name}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Obsoletes: %{name}-devel-static < %{version}-%{release}
Provides:  %{name}-devel-static = %{version}-%{release}

%description  static
Static library for gpac.

%prep
%autosetup -p1
rm -r extra_lib/
pushd share/doc
# Fix encoding warnings
cp -p ipmpx_syntax.bt ipmpx_syntax.bt.origine
iconv -f ISO-8859-1 -t UTF8 ipmpx_syntax.bt.origine >  ipmpx_syntax.bt
touch -r ipmpx_syntax.bt.origine ipmpx_syntax.bt
rm -rf share/doc/ipmpx_syntax.bt.origine
popd
sed -i 's/dh_link/ln -s -r/' Makefile


%build
%configure \
  --enable-debug \
  --extra-cflags="%{optflags} -fPIC -DPIC -D_FILE_OFFSET_BITS=64 -D_LARGE_FILES -D_LARGEFILE_SOURCE=1 -D_GNU_SOURCE=1 $(pkg-config --cflags libavformat)" \
  --X11-path=%{_prefix} \
  --libdir=%{_lib} \
  --disable-oss-audio \
%{?_with_amr:--enable-amr} \
  --disable-static \
  --use-js=no \
  --verbose

cp -p config.h include/gpac

%{make_build} all 
%{make_build} sggen

pushd share/doc
doxygen
popd

%install
%{make_install} install-lib
rm -rf %{buildroot}%{_bindir}/Osmo4

for b in MPEG4 X3D; do
  pushd applications/generators/${b}
    install -pm 0755 ${b}Gen %{buildroot}%{_bindir}
  popd
done

touch -r Changelog share/doc/html-libgpac/*

sed -i -e '/GPAC_CONFIGURATION/d' %{buildroot}%{_includedir}/gpac/configuration.h
touch -r Changelog %{buildroot}%{_includedir}/gpac/*.h
touch -r Changelog %{buildroot}%{_includedir}/gpac/internal/*.h
touch -r Changelog %{buildroot}%{_includedir}/gpac/modules/*.h
rm %{buildroot}%{_includedir}/gpac/config.h

%ldconfig_scriptlets libs

%files
%doc Changelog README.md
%license COPYING
%{_bindir}/gpac
#{_bindir}/DashCast
#{_bindir}/MP42TS
%{_bindir}/MP4Box
%{_bindir}/MP4Client
%{_bindir}/MPEG4Gen
#{_bindir}/SVGGen
%{_bindir}/X3DGen
%{_datadir}/gpac/
%{_mandir}/man1/*.1.*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/gpac.png

%files libs
%{_libdir}/libgpac.so.*
%{_libdir}/gpac/

%files doc
%doc share/doc/html-libgpac/*

%files devel
%doc share/doc/CODING_STYLE share/doc/ipmpx_syntax.bt
%{_includedir}/gpac/
%{_libdir}/libgpac.so
%{_libdir}/pkgconfig/gpac.pc

%files static
%{_libdir}/libgpac_static.a

%changelog
* Tue Nov 15 2022 hkgy <kaguyahatu@outlook.com> - 2.0.0-1
- Package init
